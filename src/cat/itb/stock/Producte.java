package cat.itb.stock;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;

public class Producte implements Serializable {
    private PropertyChangeSupport propertySupport;
    private String descripcion;
    private int idproducto;
    private int stockactual;
    private int stockminimo;
    private float pvp;

    public Producte() {
        this.propertySupport = new PropertyChangeSupport(this);
    }

    public Producte(int idproducto, String descripcion, int stockactual,
                    int stockminimo, float pvp) {
        this.propertySupport = new PropertyChangeSupport(this);
        this.idproducto = idproducto;
        this.descripcion = descripcion;
        this.stockactual = stockactual;
        this.stockminimo = stockminimo;
        this.pvp = pvp;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public int getStockminimo() {
        return this.stockminimo;
    }

    public int getIdproducto() {
        return this.idproducto;
    }

    public void setStockactual(int valorNuevo) {
        int valorAnterior = this.stockactual;
        this.stockactual = valorNuevo;
        if (this.stockactual < this.getStockminimo()) {
            this.propertySupport.firePropertyChange("stockactual",
                    valorAnterior, this.stockactual);
                    //this.stockactual = valorAnterior;
        }
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        this.propertySupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        this.propertySupport.removePropertyChangeListener(listener);
    }
}