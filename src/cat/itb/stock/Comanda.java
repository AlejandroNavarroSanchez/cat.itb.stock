package cat.itb.stock;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.Serializable;
import java.sql.*;

public class Comanda implements Serializable, PropertyChangeListener {
    private int numeropedido;
    private Producte producte;
    private Date fecha;
    private int cantidad;

    public void setNumeropedido(int numeropedido) {
        this.numeropedido = numeropedido;
    }

    public void setProducte(Producte producte) {
        this.producte = producte;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public Comanda() {}

    public Comanda(int numeropedido, Producte producte, Date fecha, int
            cantidad) {
        this.numeropedido = numeropedido;
        this.producte = producte;
        this.fecha = fecha;
        this.cantidad = cantidad;
    }

    public void propertyChange(PropertyChangeEvent evt) {
        System.out.printf("Stock anterior: %d%n", (int) evt.getOldValue());
        System.out.printf("Stock actual: %d%n", (int) evt.getNewValue());
        System.out.printf("REALIZAR PEDIDO EN PRODUCTO: %s%n", producte.getDescripcion());

        try {
            Connection conn = CloudConnection.getInstance().connect();
            Statement stmt = conn.createStatement();
            String insert = "INSERT INTO comanda VALUES (5, 59, '2020-05-15', 14)";
            stmt.executeUpdate(insert);
            stmt.close();
            conn.close();

        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState:  " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        } catch (Exception ex) {
            System.out.println("Exception: " + ex.getMessage());
        }

    }
}