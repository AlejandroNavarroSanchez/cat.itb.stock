package cat.itb.stock;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class CloudConnection {
    //informacio de la bdd
    private static String URL = "jdbc:postgresql://rogue.db.elephantsql.com:5432/";
    private static String BD = "ipysxxmu";
    private static String USER = "ipysxxmu";
    private static String PASS = "dNkB9L2UM_RFiiu-_7opQshEm1uj5IOL";

    //instancia de la bdd
    private static CloudConnection database = null;
    public static CloudConnection getInstance(){
        if(database == null)
            database = new CloudConnection();
        return database;
    }

    private Connection connection;

    //constructor
    public CloudConnection(){
        connection = null;
    }

    //metode per establir la conexio amb la bdd
    public Connection connect(){
        try {
            connection = DriverManager.getConnection(URL + BD, USER, PASS);
            System.out.println("La base de dades s'ha iniciat correctament.");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return connection;
    }

    //metode per retornar una conexio establerta
    public Connection getConnection() {
        return connection;
    }

    //metode per terminar la conexio amb la bdd
    public void close(){
        try {
            connection.close();
            System.out.println("La base de dades s'ha tancat correctament.");
        }catch(SQLException e){
            System.err.println("Error tancant la BD");
        }
        connection = null;
    }

}
